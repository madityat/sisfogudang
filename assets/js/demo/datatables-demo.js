// Call the dataTables jQuery plugin
$(document).ready(function() {
  $('#dataTable').DataTable({
    dom: 'Bfrtip',
    buttons: [{
      extend: 'print',
      text: '<i class="fa fa-print"></i> Print',
      exportOptions: {
        columns: 'th:not(.notprint)'
      }
    }]
  });
  
  ////////////////////////////

  var _link = document.createElement( 'a' );
  var _styleToAbs = function( el ) {
    var url;
    var clone = $(el).clone()[0];
    var linkHost;
  
    if ( clone.nodeName.toLowerCase() === 'link' ) {
      clone.href = _relToAbs( clone.href );
    }
  
    return clone.outerHTML;
  };

  /**
   * Convert a URL from a relative to an absolute address so it will work
   * correctly in the popup window which has no base URL.
   *
   * @param  {string} href URL
   */
  var _relToAbs = function( href ) {
    // Assign to a link on the original page so the browser will do all the
    // hard work of figuring out where the file actually is
    _link.href = href;
    var linkHost = _link.host;

    // IE doesn't have a trailing slash on the host
    // Chrome has it on the pathname
    if ( linkHost.indexOf('/') === -1 && _link.pathname.indexOf('/') !== 0) {
      linkHost += '/';
    }

    return _link.protocol+"//"+linkHost+_link.pathname+_link.search;
  };

  function formatCourseDate(date) {
    const dateObj = new Date(date + 'T00:00:00');
    return new Intl.DateTimeFormat('en-US').format(dateObj);
  }

  function sk_print( e, dt, button, config, has_child_rows ) {
    var data = dt.buttons.exportData( config.exportOptions );
    var addRow = function ( d, tag, child_row ) {
        var str = '<tr>';
        var classes = '';
        for ( var i = 0, ien = 6; i < ien; i++ ) {
            if (i == 1 || i == 4) {
              classes = 'td-header-sk';
            } else if (i == 2 || i == 5){
              classes = 'td-content-sk';
            } else {
              classes = '';
            }
            str += '<' + tag + ' class=' + classes + '>' + d[i] + '</' + tag + '>';
        }
 
        return str + '</tr>';
    };
    
    var html = '<table class="' + dt.table().node().className + ' table-sk">';
    // var header = data.header;
    var body = data.body;
    var arr = [
      [
        1, 'Merk / Type Mobil', body[0][0], 5, 'Tanggal', formatCourseDate(body[0][3])
      ],
      [
        2, 'Warna', body[0][1], 6, 'Pembawa', body[0][4]
      ],
      [
        3, 'No. Polisi', '', 7, 'Jam Keluar', body[0][5]
      ],
      [
        4, 'No. Rangka', body[0][2], 8, 'Jam Kembali', body[0][6]
      ]
    ];
    
    html += '<tbody>';
    for ( var i = 0, ien = arr.length; i < ien; i++ ) {
        // var $no_first = i+1;
        // var $no_sec = i+5;
        // if (i == 2) {
        //   arr = [
        //     $no_first, 'No. Polisi', '', $no_sec, header[i+5], body[0][i+3]
        //   ];
        // } else if (i < 2) {
        //   arr = [
        //     $no_first, header[i], body[0][i], $no_sec, header[i+5], body[0][i+3]
        //   ];
        // } else {
        //   arr = [
        //     $no_first, header[i+1], body[0][i+2], $no_sec, header[i+6], body[0][i+3]
        //   ];
        // }
        html += addRow( arr[i], 'td' );
    }
    
    html += '</tbody>';
 
    // Open a new window for the printable table
    var win = window.open();
    var title = config.title;
 
    if ( typeof title === 'function' ) {
        title = title();
    }
 
    if ( title.indexOf( '*' ) !== -1 ) {
        title = title.replace( '*', $('title').text() );
    }
    win.document.close();
    var head = '<title>'+title+'</title>';
    $('style, link').each( function() {
        head += _styleToAbs( this );
    });
 
    try {
        win.document.head.innerHTML = head; // Work around for Edge
    }
    catch (e) {
        $( win.document.head ).html( head ); // Old IE
    }
 
 
    // Inject the table and other surrounding information
    win.document.body.innerHTML =
          '<h1 class="title-sk">' + title + '</h1>'
        + '<div class="subtitle-sk">'
        +    (typeof config.message === 'function' ?
                config.message( dt, button, config ) :
                config.message
            )
        + '</div>'
        + html;
 
 
    $( win.document.body ).addClass('dt-print-view');
 
    $('img', win.document.body).each( function ( i, img ) {
        img.setAttribute( 'src', _relToAbs( img.getAttribute('src') ));
    });
 
    if ( config.customize ) {
        config.customize( win );
    }
 
    setTimeout( function() {
        if ( config.autoPrint ) {
            win.print();
            win.close();
        }
    }, 250 );
  }

  $('#dataTableSkKeluar').DataTable( {
    dom: 'Bfrtip',
    columnDefs: [ {
        orderable: false,
        className: 'select-checkbox',
        targets:   0
    } ],
    select: {
        style:    'os',
        selector: 'td:first-child'
    },
    order: [[ 1, 'asc' ]],
    buttons: [
        {
            extend: 'print',
            text: '<i class="fa fa-print"></i> Print Semua',
            exportOptions: {
                columns: 'th:not(.notprint)',
                modifier: {
                    selected: null
                }
            }
        },
        {
            extend: 'print',
            autoPrint: true,
            action : function( e, dt, button, config ) {
              sk_print( e, dt, button, config, true )
            },
            title: 'PAS - KELUAR',
            message: 'Mobil Baru / Test Car *)',
            exportOptions: {
              columns: 'th:not(.notprint)',
            },
            text: '<i class="fa fa-check"></i> Print yang terpilih',
        }
    ],
    select: true
} );
});