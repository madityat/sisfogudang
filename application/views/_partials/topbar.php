
<!-- Topbar -->
<nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

    <!-- Sidebar Toggle (Topbar) -->
          <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
            <i class="fa fa-bars"></i>
          </button>

          <!-- Topbar Search -->
          <?php 
            function parse_judul($judul) {
              $new_judul = '';
              $judul = $judul[0].' '.@$judul[1];
              switch ($judul) {
                case 'tugas ' : $new_judul = 'Penugasan SK-Keluar'; break;
                case 'datastok list' : $new_judul = 'List Data Stok'; break;
                case 'approval skmasuk' : $new_judul = 'Approval SK-Masuk'; break;
                case 'approval skkeluar' : $new_judul = 'Approval SK-Keluar'; break;
                case 'skmasuk detail' : $new_judul = 'Detail Informasi SK-Masuk'; break;
                case 'datastok ' : $new_judul = 'Tambah Data Stok'; break;
                case 'datastok edit' : $new_judul = 'Ubah Data Stok'; break;
                case 'skkeluar index' : $new_judul = 'Pembuatan Laporan SK-Keluar'; break;
                case 'skmasuk ' : $new_judul = 'Pembuatan Laporan SK-Masuk'; break;
                default : $new_judul = $judul;
              }
              return $new_judul;
            }          
            $judul = Array();
            foreach ($this->uri->segments as $segment): 
              $url = substr($this->uri->uri_string, 0, strpos($this->uri->uri_string, $segment)) . $segment;
              $is_active =  $url == $this->uri->uri_string;
              array_push($judul, $segment);
            endforeach; 
            $judul = parse_judul($judul);
            echo '
              <h1 class="h3 mb-0 text-gray-800">'.ucfirst($judul).'</h1>
              &nbsp&nbsp
            ';
          ?>

          <!-- Topbar Navbar -->
          <ul class="navbar-nav ml-auto">

            <!-- Nav Item - Search Dropdown (Visible Only XS) -->
            <li class="nav-item dropdown no-arrow d-sm-none">
              <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-search fa-fw"></i>
              </a>
              <!-- Dropdown - Messages -->
              <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in" aria-labelledby="searchDropdown">
                <form class="form-inline mr-auto w-100 navbar-search">
                  <div class="input-group">
                    <input type="text" class="form-control bg-light border-0 small" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
                    <div class="input-group-append">
                      <button class="btn btn-primary" type="button">
                        <i class="fas fa-search fa-sm"></i>
                      </button>
                    </div>
                  </div>
                </form>
              </div>
            </li>

            <!-- Nav Item - Alerts -->
            <li class="nav-item dropdown no-arrow mx-1">
                <?php
                  $sql = 'select * from user_notifikasi where (untuk = 0 or untuk = '. $this->session->userdata['_user_id'].') AND isRead = 0 order by id_notifikasi DESC';
                  $result = $this->db->query($sql);
                ?>
              <a class="nav-link dropdown-toggle" href="#" id="alertsDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-bell fa-fw"></i>
                <!-- Counter - Alerts -->
                <span class="badge badge-danger badge-counter"><?php echo $result->num_rows(); ?></span>
              </a>
              <!-- Dropdown - Alerts -->
              <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="alertsDropdown">
                <h6 class="dropdown-header">
                  Notifikasi
                </h6>
                <?php
                  foreach ($result->result() as $notif):
                    $time = strtotime($notif->createdAt);
                    ?>
                    <a class="dropdown-item d-flex align-items-center" href="<?php echo site_url('notifikasi/index/').$notif->id_notifikasi; ?>">
                      <div class="mr-3">
                        <div class="icon-circle bg-<?php echo $notif->tipe; ?>">
                          <i class="fas fa-file-alt text-white"></i>
                        </div>
                      </div>
                      <div>
                        <div class="small text-gray-500"><?php echo date('d M Y, H:i:s',$time);?></div>
                        <span class="font-weight-bold"><?php echo $notif->pesan; ?></span>
                      </div>
                    </a>
                    <?php
                  endforeach;
                ?>
              </div>
            </li>

            <div class="topbar-divider d-none d-sm-block"></div>

            <!-- Nav Item - User Information -->
            <li class="nav-item dropdown no-arrow">
              <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="mr-2 d-none d-lg-inline text-gray-600 small"><?php echo $this->session->userdata('_name'); ?></span>
              </a>
              <!-- Dropdown - User Information -->
              <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                <a class="dropdown-item" href="<?php echo site_url('Auth/logout');?>" data-toggle="modal" data-target="#logoutModal">
                  <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                  Logout
                </a>
              </div>
            </li>

          </ul>

        </nav>
        <!-- End of Topbar -->