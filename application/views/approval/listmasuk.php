  <!-- DataTables -->
  <div class="card mb-3">
		<div class="card-body">
			<div class="table-responsive">
				<table class="table table-hover table-bordered" id="dataTable" width="100%" cellspacing="0">
					<thead>
						<tr>
							<th>ID</th>
							<th>Tipe Mobil</th>
							<th>Warna Mobil</th>
							<th>No Rangka</th>
							<th>Asal Mobil</th>
							<th>Tanggal Masuk</th>
							<th>Pembawa</th>
							<th>Jam Tiba</th>
							<th>Keterangan</th>
							<th>Status</th>
							<?php
								if($this->session->userdata['_type'] === 'pic' || $this->session->userdata['_type'] === 'admin') {
									?>
										<th style="" class="notprint">AKSI</th>
									<?php
								}
							?>
						</tr>
					</thead>
					<tbody>
						<?php 
							foreach ($list as $dt): 
						?>
						<tr>
							<td>
								<?php echo $dt['id_sk_masuk']; ?>
							</td>
							<td>
								<?php echo $dt['tipe_mobil']; ?>
							</td>
							<td>
								<?php echo $dt['warna_mobil']; ?>
							</td>
							<td>
								<?php echo $dt['nomor_rangka']; ?>
              				</td>
							<td>
								<?php echo $dt['asal_mobil']; ?>
							</td>
							<td>
								<?php echo $dt['tanggal']; ?>
							</td>
							<td>
								<?php echo (isset($dt['name']) ? $dt['name'] : ''); ?>
							</td>
							<td>
								<?php echo $dt['jam_tiba']; ?>
							</td>
							<td>
								<?php echo $dt['keterangan']; ?>
							</td>
							<td>
								<?php echo $dt['status']; ?>
							</td>
							<?php
								if($this->session->userdata['_type'] === 'pic' || $this->session->userdata['_type'] === 'admin') {
									?>
										<td>
											<a href="<?php echo site_url('skmasuk/detail/').$dt['id_sk_masuk'];?>"><button class="btn btn-small btn-primary">LIHAT PENGAJUAN</button></a>
										</td>
									<?php
								}
							?>
						</tr>
						<?php 
							endforeach;
						?>
					</tbody>
                </table>
			</div>
		</div>
	</div>

	<script>
	function deleteConfirm(url){
		$('#btn-delete').attr('href', url);
		$('#deleteModal').modal();
	}
	</script>