  <!-- DataTables -->
  <div class="card mb-3">
		<div class="card-body">
			<div class="table-responsive">
				<table class="table table-hover table-bordered" id="dataTableSkKeluar" width="100%" cellspacing="0">
					<thead>
						<tr>
							<th class="notprint"></th>
							<th class="notprint">ID</th>
							<th>Type Mobil</th>
							<th>Warna Mobil</th>
							<th>No Rangka</th>
							<th>Peruntukan</th>
							<th>Permintaan Dari</th>
							<th>Tanggal Keluar</th>
							<th>Pembawa</th>
							<th>Jam Keluar</th>
							<th>Jam Masuk</th>
							<th class="notprintselected">Status</th>
							<th class="notprintselected">keterangan</th>
							<th style="" class="notprint">AKSI</th>
						</tr>
					</thead>
					<tbody>
						<?php 
							foreach ($list as $dt): 
						?>
						<tr>
							<td></td>
							<td>
								<?php echo $dt['id_sk_keluar']; ?>
							</td>
							<td>
								<?php echo $dt['tipe_mobil']; ?>
							</td>
							<td>
								<?php echo $dt['warna_mobil']; ?>
							</td>
							<td>
								<?php echo $dt['nomor_rangka']; ?>
              				</td>
							<td>
								<?php echo $dt['peruntukan']; ?>
              				</td>
							<td>
								<?php echo $dt['permintaan_dari']; ?>
              				</td>
							<td>
								<?php echo $dt['tanggal']; ?>
							</td>
							<td>
								<?php echo (isset($dt['name']) ? $dt['name'] : ''); ?>
							</td>
							<td>
								<?php echo $dt['jam_keluar']; ?>
              				</td>
							<td>
								<?php echo $dt['jam_kembali']; ?>
							</td>
							<td>
								<?php echo $dt['status']; ?>
							</td>
							<td>
								<?php echo $dt['keterangan']; ?>
							</td>
							<td width="175">
							<?php
								if($dt['status'] === 'TELAH DIBUAT') {
									?>
										<a href="<?php echo site_url('skkeluar/approve/').$dt['id_sk_keluar'].'/'.$dt['nomor_rangka'];?>"><button class="btn btn-sm btn-success">APPROVE</button></a>
									<?php
								}
								if($this->session->userdata['_type'] === 'admin') {
									?>
										<a href="<?php echo site_url('skkeluar/index/').$dt['id_sk_keluar'];?>"><button class="btn btn-small btn-primary">LIHAT PENGAJUAN</button></a>
									<?php
								} else {
									if ($dt['status'] === 'TELAH DIBUAT') {
									?>
										<a href="<?php echo site_url('skkeluar/tolaksk/').$dt['id_sk_keluar'].'/'.($dt['nomor_rangka'] ? $dt['nomor_rangka'] : '---').'/id_admin';?>"><button class="btn btn-small btn-danger">REJECT</button></a>
									<?php
									} else if ($dt['status'] === 'MENUNGGU DIBUAT ADMIN') {
									?>
										<a href="<?php echo site_url('skkeluar/tolaksk/').$dt['id_sk_keluar'].'/'.($dt['nomor_rangka'] ? $dt['nomor_rangka'] : '---').'/id_admin';?>"><button class="btn btn-small btn-danger">CANCEL</button></a>
									<?php
									}
								}
							?>
							</td>
						</tr>
						<?php 
							endforeach;
						?>
					</tbody>
        </table>
			</div>
		</div>
	</div>

	<script>
	function deleteConfirm(url){
		$('#btn-delete').attr('href', url);
		$('#deleteModal').modal();
	}
	</script>