<?php if ($this->session->flashdata('success')): ?>
<div class="alert alert-success" role="alert">
	<?php echo $this->session->flashdata('success'); ?>
</div>
<?php endif; ?>
<?php if ($this->session->flashdata('error')): ?>
<div class="alert alert-danger" role="alert">
	<?php echo $this->session->flashdata('error'); ?>
</div>
<?php endif; ?>
  <!-- DataTables -->
  <div class="card mb-3">
		<div class="card-body">
			<div class="table-responsive">
				<table class="table table-hover" id="dataTable" width="100%" cellspacing="0">
					<thead>
						<tr>
							<th>NO</th>
							<th>Type Mobil</th>
							<th>Warna Mobil</th>
							<th>Nomor Rangka</th>
							<?php 
								if ($this->session->userdata['_type'] === 'admin') {
									echo '<th class="notprint">Aksi</th>';
								}
							?>							
						</tr>
					</thead>
					<tbody>
						<?php
							$no = 1; 
							foreach ($list->result() as $dt): ?>
						<tr>
							<td>
								<?php echo $no++; ?>
							</td>
							<td>
								<?php echo $dt->tipe_mobil; ?>
							</td>
							<td>
								<?php echo $dt->warna_mobil; ?>
				             </td>
							<td>
								<?php echo $dt->nomor_rangka; ?>
            				</td>
							<?php 
								if ($this->session->userdata['_type'] === 'admin') {
									?>
										<td width="250">
											<a href="<?php echo site_url('datastok/edit/').$dt->nomor_rangka; ?>" class="btn btn-info btn-small"><i class="fas fa-edit"></i> Edit</a>
											<a onClick="return confirm('Are you sure you want to delete?')" href='<?php echo site_url('datastok/delete/').$dt->nomor_rangka; ?>' class='btn btn-small btn-danger'><i class="fas fa-trash"></i> delete</a>
										</td>
									<?php
								}
							?>							
						</tr>
						<?php endforeach; ?>
					</tbody>
        </table>
			</div>
		</div>
	</div>

<script>
	function deleteConfirm(url){
		$('#btn-delete').attr('href', url);
		$('#deleteModal').modal();
	}
</script>