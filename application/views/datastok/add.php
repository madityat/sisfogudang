<?php if ($this->session->flashdata('success')): ?>
<div class="alert alert-success" role="alert">
	<?php echo $this->session->flashdata('success'); ?>
</div>
<?php endif; ?>
<?php if ($this->session->flashdata('error')): ?>
<div class="alert alert-danger" role="alert">
	<?php echo $this->session->flashdata('error'); ?>
</div>
<?php endif; ?>

<div class="card mb-3">
    <div class="card-body">
        <form method="post" action="<?php echo site_url('datastok/add')?> ">
            <div class="form-group">
            <label for="name">Tipe Mobil*</label>
                <select name="tipe_mobil" id="tipe_mobil" class="form-control" onchange="getWarnaMobil()" required>
                    <option value="" disabled selected>PILIH TIPE MOBIL</option>
                    <?php foreach ($list_tipe_mobil->result() as $data): ?>
                        <option value="<?php echo $data->tipe_mobil; ?>"><?php echo $data->tipe_mobil; ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div class="form-group">
              <label for="name">Warna Mobil*</label>
                <select name="warna_mobil" id="warna_mobil" class="form-control"required>
                    <option value="" disabled selected>PILIH WARNA MOBIL</option>
                </select>
            </div>
            <div class="form-group">
              <label for="name">No. Rangka*</label>
                <input class="form-control" required type="text" name="nomor_rangka"/>
            </div>
            <input class="btn btn-success" type="submit" value="TAMBAH">
        </form>
    </div>
</div>

<div class="card-footer small text-muted">
	* Harus Diisi
</div>
<script>
  function getWarnaMobil() {
      var tipe_mobil = document.getElementById("tipe_mobil").value;
      var warna_mobil = $('#warna_mobil');
      $.ajax({
          type: "GET",
          url: '<?php echo site_url('tugas/warnaMobil')?>/'+tipe_mobil,
          success: function(data){
              warna_mobil.html(data);                
          }
      });
  }
</script>
