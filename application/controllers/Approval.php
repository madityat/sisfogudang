<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Approval extends CI_Controller {
	public function __construct(){
		parent::__construct();
		if (!$this->login_model->logged_id()){
			redirect('Auth','refresh');
		}
	}

	public function index()
	{
		redirect('approval/skmasuk','refresh');
	}

	public function skmasuk() {
		$var = $this->session->userdata;

		$data['page'] = 'approval/listmasuk';
		$data['sidebar'] = $var['_type'];
		$this->db->select('*');
		$this->db->from('sk_masuk');
		if ($var['_type'] === 'security') {
			$this->db->where('id_security', $var['_user_id']);
		}
		if ($var['_type'] === 'admin') {
			$this->db->where('status', 'MENUNGGU APPROVAL ADMIN');
		}
		if ($var['_type'] === 'pic') {
			$this->db->where('status', 'MENUNGGU APPROVAL PIC');
		}
		$this->db->join('users', 'sk_masuk.pembawa = users.id');
		$results1 = $this->db->get()->result_array();

		$this->db->select('*');
		$this->db->from('sk_masuk');
		$this->db->where('pembawa', NULL);
		if ($var['_type'] === 'security') {
			$this->db->where('id_security', $var['_user_id']);
		}
		if ($var['_type'] === 'admin') {
			$this->db->where('status', 'MENUNGGU APPROVAL ADMIN');
		}
		if ($var['_type'] === 'pic') {
			$this->db->where('status', 'MENUNGGU APPROVAL PIC');
		}
		$results2 = $this->db->get()->result_array();

		$data['list'] = array_merge($results1,$results2);

		$this->load->view('_partials/template', $data);
	}

	public function skkeluar() {
		$data['page'] = 'approval/listkeluar';
		$data['sidebar'] = $this->session->userdata['_type'];
		$this->db->select('*');
		$this->db->from('sk_keluar');
		$this->db->join('users', 'sk_keluar.pembawa = users.id');
		if ($this->session->userdata['_type'] == 'admin') {
			$this->db->where('sk_keluar.status', 'MENUNGGU DIBUAT ADMIN');
		}
		$results1 = $this->db->get()->result_array();

		$this->db->select('*');
		$this->db->from('sk_keluar');
		$this->db->where('pembawa', NULL);
		if ($this->session->userdata['_type'] == 'admin') {
			$this->db->where('sk_keluar.status', 'MENUNGGU DIBUAT ADMIN');
		}
		$results2 = $this->db->get()->result_array();

		$data['list'] = array_merge($results1,$results2);

		$this->load->view('_partials/template', $data);
	}
}