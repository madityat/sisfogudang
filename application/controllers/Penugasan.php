<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Penugasan extends CI_Controller {
	public function __construct(){
		parent::__construct();
		if (!$this->login_model->logged_id()){
			redirect('Auth','refresh');
		}
	}
	
	public function index()
	{
		$data['page'] = 'penugasan/list';
		$data['sidebar'] = $this->session->userdata['_type'];
		$data['list'] = $this->db->get("penugasan_sk_keluar");
		$this->load->view('_partials/template', $data);
	}
}