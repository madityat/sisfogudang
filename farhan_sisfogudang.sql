-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 13, 2019 at 04:56 PM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `farhan_sisfogudang`
--

-- --------------------------------------------------------

--
-- Table structure for table `data_stok`
--

CREATE TABLE `data_stok` (
  `tipe_mobil` varchar(20) NOT NULL,
  `warna_mobil` varchar(20) NOT NULL,
  `nomor_rangka` varchar(50) NOT NULL,
  `stok` int(5) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `data_stok`
--

INSERT INTO `data_stok` (`tipe_mobil`, `warna_mobil`, `nomor_rangka`, `stok`) VALUES
('Jazz Turbo', 'Hitam', '11Q2W3JI2JDWIU', 1),
('Mobil Baru', 'Jingga Hitam', '1Q2W', 0),
('Mobil Baru', 'Jingga Hitam', '1Q2W5', 1),
('Jazz RS', 'Merah', '9012919993', 0),
('Jazz Turbo', 'Hitam', '901292992', 0),
('Jazz Turbo', 'Hijau', '9013929921', 1),
('Jazz RS', 'Hitam', '9019249999', 1),
('Jazz Turbo', 'Hitam', '90192992', 1),
('Jazz Turbo', 'Hijau', '901929921', 1),
('Jazz RS', 'Hitam', '901929999', 1),
('Becak', 'Putih', 'B12HD6JKW90208920', 1),
('Mobil Baru', 'Jingga', 'EEQW21', 1),
('Becak', 'Ungu', 'EEQW21212', 1),
('Mobil Baru', 'Jingga', 'EEQW213', 1);

-- --------------------------------------------------------

--
-- Table structure for table `sk_keluar`
--

CREATE TABLE `sk_keluar` (
  `id_sk_keluar` int(5) NOT NULL,
  `id_pic` int(11) NOT NULL,
  `id_admin` int(11) DEFAULT NULL,
  `tipe_mobil` varchar(20) NOT NULL,
  `warna_mobil` varchar(20) NOT NULL,
  `nomor_rangka` varchar(50) DEFAULT NULL,
  `peruntukan` varchar(20) NOT NULL,
  `permintaan_dari` varchar(50) NOT NULL,
  `tanggal` date NOT NULL,
  `pembawa` int(11) DEFAULT NULL,
  `jam_keluar` time NOT NULL,
  `jam_kembali` varchar(15) DEFAULT NULL,
  `status` varchar(50) NOT NULL DEFAULT 'MENUNGGU DIBUAT ADMIN',
  `keterangan` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sk_keluar`
--

INSERT INTO `sk_keluar` (`id_sk_keluar`, `id_pic`, `id_admin`, `tipe_mobil`, `warna_mobil`, `nomor_rangka`, `peruntukan`, `permintaan_dari`, `tanggal`, `pembawa`, `jam_keluar`, `jam_kembali`, `status`, `keterangan`) VALUES
(1, 3, 1, 'Jazz RS', 'Merah', '9012919993', 'dibeli', 'Main Dealer', '2019-08-14', 5, '12:00:00', '', 'TELAH DIBUAT', 'good'),
(2, 3, NULL, 'Jazz RS', 'Merah', NULL, 'dipinjam', 'Dealer', '2019-08-13', NULL, '11:11:00', '', 'MENUNGGU DIBUAT ADMIN', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sk_masuk`
--

CREATE TABLE `sk_masuk` (
  `id_sk_masuk` int(10) NOT NULL,
  `id_security` int(11) NOT NULL,
  `tipe_mobil` varchar(20) NOT NULL,
  `warna_mobil` varchar(20) NOT NULL,
  `nomor_rangka` varchar(50) NOT NULL,
  `asal_mobil` varchar(20) NOT NULL,
  `tanggal` date NOT NULL,
  `pembawa` varchar(20) NOT NULL,
  `jam_tiba` time NOT NULL,
  `keterangan` varchar(140) NOT NULL,
  `status` varchar(50) NOT NULL DEFAULT 'MENUNGGU APPROVAL ADMIN'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sk_masuk`
--

INSERT INTO `sk_masuk` (`id_sk_masuk`, `id_security`, `tipe_mobil`, `warna_mobil`, `nomor_rangka`, `asal_mobil`, `tanggal`, `pembawa`, `jam_tiba`, `keterangan`, `status`) VALUES
(1, 2, 'Jazz Turbo', 'Hitam', '11Q2W3JI2JDWIU', 'Dealer', '2019-08-14', '4', '10:10:00', 'MANTAP', 'APPROVED');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `type` enum('admin','pic','security','driver') NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `type`, `username`, `password`, `email`) VALUES
(1, 'Admin', 'admin', 'admin', '21232f297a57a5a743894a0e4a801fc3', 'farhanasyahman@gmail.com'),
(2, 'Security', 'security', 'security', 'e91e6348157868de9dd8b25c81aebfb9', 'fadlimaulanaaa@gmail.com'),
(3, 'pic', 'pic', 'pic', 'ed09636a6ea24a292460866afdd7a89a', 'difairawan44@gmail.com'),
(4, 'Difa Driver', 'driver', '', '', 'farhanasyahman@gmail.com'),
(5, 'Fadli Driver', 'driver', '', 'fadlimaulanaaa@gmail.com ', 'farhanasyahman@gmail.com'),
(6, 'pic2', 'pic', 'pic2', 'ed09636a6ea24a292460866afdd7a89a', 'farhanasyahman@gmail.com'),
(7, 'pic3', 'pic', 'pic3', 'e91e6348157868de9dd8b25c81aebfb9', 'fadlimaulanaaa@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `user_notifikasi`
--

CREATE TABLE `user_notifikasi` (
  `id_notifikasi` int(11) NOT NULL,
  `dari` varchar(25) NOT NULL,
  `untuk` int(11) NOT NULL,
  `pesan` varchar(500) NOT NULL,
  `tipe` varchar(11) NOT NULL DEFAULT 'primary',
  `isRead` int(11) NOT NULL DEFAULT '0',
  `createdAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `user_notifikasi`
--

INSERT INTO `user_notifikasi` (`id_notifikasi`, `dari`, `untuk`, `pesan`, `tipe`, `isRead`, `createdAt`) VALUES
(19, 'Security', 3, 'SK Masuk telah dibuat oleh Security', 'info', 0, '2019-08-07 14:18:20'),
(20, 'Security', 6, 'SK Masuk telah dibuat oleh Security', 'info', 0, '2019-08-07 14:18:22'),
(21, 'Security', 7, 'SK Masuk telah dibuat oleh Security', 'info', 0, '2019-08-07 14:18:25'),
(22, 'Security', 3, 'SK Masuk telah dibuat oleh Security', 'info', 0, '2019-08-07 14:18:52'),
(23, 'Security', 6, 'SK Masuk telah dibuat oleh Security', 'info', 0, '2019-08-07 14:18:55'),
(24, 'Security', 7, 'SK Masuk telah dibuat oleh Security', 'info', 0, '2019-08-07 14:18:58'),
(25, 'Security', 3, 'SK Masuk telah dibuat oleh Security', 'info', 0, '2019-08-13 14:01:35'),
(26, 'Security', 6, 'SK Masuk telah dibuat oleh Security', 'info', 0, '2019-08-13 14:01:39'),
(27, 'Security', 7, 'SK Masuk telah dibuat oleh Security', 'info', 0, '2019-08-13 14:01:43'),
(28, 'Security', 3, 'SK Masuk telah dibuat oleh Security', 'info', 0, '2019-08-13 14:06:15'),
(29, 'Security', 6, 'SK Masuk telah dibuat oleh Security', 'info', 0, '2019-08-13 14:06:18'),
(30, 'Security', 7, 'SK Masuk telah dibuat oleh Security', 'info', 0, '2019-08-13 14:06:20'),
(31, 'Admin', 2, 'Pengajuan SK Masuk dengan id 1 telah di approve oleh PIC', 'success', 0, '2019-08-13 14:12:26'),
(32, 'Admin', 2, 'Pengajuan SK Masuk dengan id 1 telah di approve oleh PIC', 'success', 0, '2019-08-13 14:16:17'),
(33, 'Admin', 2, 'Pengajuan SK Masuk dengan id 1 telah di approve oleh PIC', 'success', 0, '2019-08-13 14:17:26'),
(34, 'pic', 2, 'Pengajuan SK Masuk dengan id 1 telah di approve oleh PIC', 'success', 0, '2019-08-13 14:25:47'),
(35, 'pic', 2, 'Pengajuan SK Masuk dengan id 1 telah di approve oleh PIC', 'success', 0, '2019-08-13 14:27:18'),
(36, 'Admin', 3, 'Pengajuan SK Keluar dengan id 1 telah dibuat oleh admin', 'info', 0, '2019-08-13 14:44:58');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `data_stok`
--
ALTER TABLE `data_stok`
  ADD PRIMARY KEY (`nomor_rangka`);

--
-- Indexes for table `sk_keluar`
--
ALTER TABLE `sk_keluar`
  ADD PRIMARY KEY (`id_sk_keluar`),
  ADD KEY `id_pic` (`id_pic`),
  ADD KEY `id_admin` (`id_admin`);

--
-- Indexes for table `sk_masuk`
--
ALTER TABLE `sk_masuk`
  ADD PRIMARY KEY (`id_sk_masuk`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_notifikasi`
--
ALTER TABLE `user_notifikasi`
  ADD PRIMARY KEY (`id_notifikasi`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `sk_keluar`
--
ALTER TABLE `sk_keluar`
  MODIFY `id_sk_keluar` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `sk_masuk`
--
ALTER TABLE `sk_masuk`
  MODIFY `id_sk_masuk` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `user_notifikasi`
--
ALTER TABLE `user_notifikasi`
  MODIFY `id_notifikasi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
